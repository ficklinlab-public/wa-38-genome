Download the 26Gv2.0 database for PlantTribes2  
```bash
wget http://bigdata.bx.psu.edu/PlantTribes_scaffolds/data/26Gv2.0.tar.bz2
```

unzip the file  
```bash
tar -xzvf 26Gv2.0.tar.bz2
```

You should have one directory here named `26Gv2.0`  
 
