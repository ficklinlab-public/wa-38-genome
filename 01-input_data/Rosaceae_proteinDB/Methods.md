## Download protein files from selected Rosaceae genomes  
```bash
#Fragaria_vesca
wget https://www.rosaceae.org/rosaceae_downloads/Fragaria_vesca/Fvesca-genome.v4.0.a2/genes/Fragaria_vesca_v4.0.a2.proteins.fa.gz
#Malus_baccata
wget https://www.rosaceae.org/rosaceae_downloads/Malus_baccata/mbaccata_v1.0/genes/Malus_baccata_v1.0_pep.fasta.gz
#Malus_domestica_Gala_haploid
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Gala_haploid_v1/genes/Gala_haploid_v2.pep.fa.gz
#Malus_domestica_GDDH13
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/genes/GDDH13_1-1_prot.fasta.gz
#Malus_domestica_Honeycrisp_HAP1
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
#Malus_domestica_Honeycrisp_HAP2
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP2_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
#Malus_sieversii_haploid
wget https://www.rosaceae.org/rosaceae_downloads/Malus_sieversii/Msieversii_haploid_v1/genes/Msieversii_haploid_v2.pep.fa.gz
#Malus_sylvestris_haploid
wget https://www.rosaceae.org/rosaceae_downloads/Malus_sylvestris/Msylvestris_haploid_v1/genes/Msylvestris_haploid_v2.pep.fa.gz
#Prunus_persica_v2.0.a1
wget https://www.rosaceae.org/rosaceae_downloads/Prunus_persica/Prunus_persica-genome.v2.0.a1/genes/Prunus_persica_v2.0.a1.primaryTrs.pep.fa.gz
#Pyrus_betulifolia
wget https://www.rosaceae.org/rosaceae_downloads/Pyrus_betulifolia/pbetulifolia_v1.0/genes/GWHAAYT00000000.Protein.faa.gz
#Pyrus_communis_DAnjou_v0.1
wget https://www.rosaceae.org/rosaceae_downloads/Pyrus_communis/pcommunis_DAnjou_v1.0/genes/6.Pyrus_communis_DAnjou_chr.v0.1.proteins.fasta.gz
#Pyrus_pyrifolia_Nijisseiki
wget https://www.rosaceae.org/rosaceae_downloads/Pyrus_pyrifolia/ppyrifolia_v1.0/genes/PPY_r1.0.pep.fasta.gz
#Rosa_chinensis_Old_Blush_homozygous
wget https://www.rosaceae.org/rosaceae_downloads/Rosa_chinensis/Rchinensis_Old_Blush_homozygous_genome-v2.0/genes/Rosa_chinensis_Old_Blush_homozygous_genome-v2.0.a1.prot.faa.gz
#Rubus_occ_V3
wget https://www.rosaceae.org/rosaceae_downloads/Rubus_occidentalis/Rubus_occidentalis-genome.v3.0/genes/Rubus_occ_V3.proteins.fasta.gz

gunzip *.gz
```
