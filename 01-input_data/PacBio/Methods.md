## Download data  
The PacBio data for 'WA 38' genome assembly is available on NCBI under BioProject PRJNA1072127, Run SRR27841236  

The original files names from the sequencing facility were used in this workflow:  
- m64233e_211117_203327.fastq.gz  
- m64233e_211123_195103.fastq.gz  

