## Data Source  
Hi-C data for the 'WA 38' genome is available under BioProject PRJNA1072127, Run SRR27841235  
Please download the data prior to executing the workflow.  

The name of the original files from the sequencing facility are:  
- JNQM_OmniC_NA_NA_CTTGTCGA_Apple_Cosmic_Crisp-Apple_WA38_Cosmic_OmniC_I1161L1_L1_R1.fastq  
- JNQM_OmniC_NA_NA_CTTGTCGA_Apple_Cosmic_Crisp-Apple_WA38_Cosmic_OmniC_I1161L1_L1_R2.fastq  

Those file names were used in this workflow.  

