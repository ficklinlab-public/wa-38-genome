## Download data  
The shotgun DNA reads is availbale on NCBI under BioProject PRJNA1072127, run SRR27841234.  

The original sequencing names were used in this workflow:  
- JPQN_PCRfree_1_1_GGCTTAAG_Apple_WA38_I1177_L4_R1.fastq.gz  
- JPQN_PCRfree_1_1_GGCTTAAG_Apple_WA38_I1177_L4_R2.fastq.gz  

Please download the data before running the workflow.  
