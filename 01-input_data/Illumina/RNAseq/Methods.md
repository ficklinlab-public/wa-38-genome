## Download data  
The RNA seq data is available under BioProject PRJNA791346.  
The 'WA 38' samples are SAMN29611978 to SAMN29611985.  
We used their original sequencing names in this workflow which are listed below:  
- JLHJ_RNAseq_002B_Fruitlet_Stage_1_ACTAAGAT_Apple_WA38_I1126_L1_R1.fastq  
- JLHJ_RNAseq_002B_Fruitlet_Stage_1_ACTAAGAT_Apple_WA38_I1126_L1_R2.fastq  
- JLHK_RNAseq_008C_Fruitlet_Stage_2_GTCGGAGC_Apple_WA38_I1126_L1_R1.fastq  
- JLHK_RNAseq_008C_Fruitlet_Stage_2_GTCGGAGC_Apple_WA38_I1126_L1_R2.fastq  
- JLHL_RNAseq_0013_Budding_Leaves_CTTGGTAT_Apple_WA38_I1126_L1_R1.fastq  
- JLHL_RNAseq_0013_Budding_Leaves_CTTGGTAT_Apple_WA38_I1126_L1_R2.fastq  
- JLHM_RNAseq_0018_Expanding_Leaves_TCCAACGC_Apple_WA38_I1126_L1_R1.fastq  
- JLHM_RNAseq_0018_Expanding_Leaves_TCCAACGC_Apple_WA38_I1126_L1_R2.fastq  
- JLHN_RNAseq_0023C_Roots_from_tissue_culture_CCGTGAAG_Apple_WA38_I1126_L1_R1.fastq  
- JLHN_RNAseq_0023C_Roots_from_tissue_culture_CCGTGAAG_Apple_WA38_I1126_L1_R2.fastq  
- JLHP_RNAseq_0026_Quarter_inch_Buds_TTACAGGA_Apple_WA38_I1126_L1_R1.fastq  
- JLHP_RNAseq_0026_Quarter_inch_Buds_TTACAGGA_Apple_WA38_I1126_L1_R2.fastq  
- JLHQ_RNAseq_0031_Flower_Buds_GGCATTCT_Apple_WA38_I1126_L1_R1.fastq  
- JLHQ_RNAseq_0031_Flower_Buds_GGCATTCT_Apple_WA38_I1126_L1_R2.fastq  
- JLHR_RNAseq_0036B_Open_Buds_AATGCCTC_Apple_WA38_I1126_L1_R1.fastq  
- JLHR_RNAseq_0036B_Open_Buds_AATGCCTC_Apple_WA38_I1126_L1_R2.fastq  

Please download the data before running the workflow.  

