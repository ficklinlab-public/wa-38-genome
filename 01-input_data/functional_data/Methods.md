## Download funtional database  
EnTAP provide a function to download functional databases.  
```bash
sbatch 01-EnTAP_download.srun
```

## A list if directories   
- EnTAPnf  
- interproscan  
- refseq  
- uniprot_sprot  
