Directory Overview
==================
This directory contains steps of running functional annotation using 
[EnTAPnf](https://github.com/SystemsGenetics/EnTAPnf).  
Authors: Huiting Zhang  
Last Updated: Dec 14th 2023  

Directory Structure
===================
The following describes the purpose for each directory:
### 01-EnTAP
- 01-hapA
    * 01-interpretation  
    *This directory contains analysis of the EnTAP results*
- 02-hapB
    * 01-interpretation  
    *This directory contains analysis of the EnTAP results*
