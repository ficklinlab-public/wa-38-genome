### Run EnTAP

First, we will remove the * at the end of the protein sequences to create a new file for functional annotation.
```bash
cat ../../../05-structural_annotation/06-annotate-genes/08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa | sed 's/*//' > Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.cleaned.faa
```

Since there were issues running EnTAP with relative PATH or sym linked files, absolute path was used for the functional data files in the slurm script.
```bash
sbath 01-entap_hapA.srun
```

### Results intepretation

First, make a new directory in which we will analyze the output from EnTAP
```bash
mkdir 01-interpretation
```
