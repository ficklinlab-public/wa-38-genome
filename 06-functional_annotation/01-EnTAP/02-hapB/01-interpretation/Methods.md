The goal of this analysis is to calculate how many proteins were assigned with functional terms from each program (and all the programs) used by EnTAP.

First, count the number of contaminants
```bash
grep -c ">" ../results/EnTAP/outfiles/final_results/final_annotations_contam.faa 
#0 This is no contamination detected by EnTAP
```
Next, count the number of annotated and unannotated proteins
```bash
grep -c ">" ../results/EnTAP/outfiles/final_results/final_annotated.faa 
#62244
grep -c ">" ../results/EnTAP/outfiles/final_results/final_unannotated.faa      
#7815
```

The percentage of genes with annotation (from one or more tools) is: 62244/(62244+7815)=88.8%  

Finally, count how many genes were annotated by each tool  

Since the Uniprot and InterProScan results are incorrectly incorporated into the final result table, let's remove those columns and make a new table.
```bash
cat ../results/EnTAP/outfiles/final_results/final_annotations_no_contam_lvl0.tsv | cut -f1-18,25-37 > Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv
```

Now seperate results from each tool
```bash
cut -f2 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   58170. 58169 proteins are annotated by BLAST (the count of 58170 included header line)
cut -f19 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   60334. 60333 proteins are annotated by EggNOG seed ortholog search
cut -f27 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   14033. 14032 proteins are annotated by EggNOG KEGG search
cut -f28 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   42568. 42567 proteins are annotated by EggNOG GO biological process search
cut -f29 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   36565. 36564 proteins are annotated by EggNOG GO cellular component search
cut -f30 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   40901. 40900 proteins are annotated by EggNOG GO molecular function search
cut -f31 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.EnTAP.annotation.tsv | sed '/^\s*$/d' | wc -l
#   56561. 56560 proteins are annotated by EggNOG Protein Domain search
```

The output from InterProScan was integrated incorrectly in the final file, therefore, we need to analyze that using the files from the InterProScan output directory
```bash
ln -s ../results/InterProScan/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.GO_mappings.txt
ln -s ../results/InterProScan/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.IPR_mappings.txt 
ln -s ../results/InterProScan/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.tsv  
```

Count how many genes have GO term annotation
```bash
cut -f1 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.GO_mappings.txt| grep Maldo | sort | uniq | wc -l
#   35350
```

Count how many genes have IPR terms
```bash
cut -f1 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.IPR_mappings.txt | grep Maldo | sort | uniq | wc -l 
#   50462
```

Count how many genes have hits in all the databases searched by InterProScan
```bash
cut -f1 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.tsv | grep Maldo | sort | uniq | wc -l 
#   59353
```

The output from Uniprot was also integrated incorrectly in the final file, therefore, we need to analyze that using the files from the DIAMOND output directory. 
```bash
ln -s ../results/DIAMOND/blastp_Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.faa_final_uniprot_sprot.out
```

Count how many genes have hits in the uniprot database
```bash
cut -f1 blastp_Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.cleaned.faa_final_uniprot_sprot.out | grep Maldo | sort | uniq| wc -l
# 37011
```
