### Data preparation

Create sym links to the proteins.both.26Gv2.0.bestOrthos.summary files from each classified annotation:
```bash
ln -s ../01-classification/01-hapA/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary WA-38.hapA.orthogroup.classification
ln -s ../01-classification/02-hapB/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary WA-38.hapB.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/01-Honeycrisp-hapA/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Honeycrisp.hapA.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/02-Honeycrisp-hapB/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Honeycrisp.hapB.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/03-Malus-baccata/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Malba.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/04-Malus-fusca-hapA/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Malfu.hapA.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/05-Malus-fusca-hapB/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Malfu.hapB.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/06-Malus-prunifolia/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Malpr.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/07-dAnjou-hapA/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Pyrco.da.hapA.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/08-dAnjou-hapB/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Pyrco.da.hapB.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/09-Gala/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Gala.hap.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/10-Malus-sieversii/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Malsi.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/11-Malus-sylvestris/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Malsy.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/12-HanFu/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary HanfuTH.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/13-GDDH13/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary GDDH13.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/14-Antonovka-hapA/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Anton.hapA.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/15-Antonovka-hapB/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Anton.hapB.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/16-Fuji/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary Fuji.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/17-M9/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary M19.orthogroup.classification
ln -s ../01-classification/03-Other_Malus_Pyrus/18-MM106-hapA/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary MM106.orthogroup.classification
```

We will create 2 sets of lists using these data -  the first one summarizes which orthogroups (OGs) has genes assigned to from each annotation; the second one has gene counts from each annotation in each CoRe OrthoGroup - Rosaceae. The first set of lists will be used to make upset plot comparing shared and unique orthogroups numebrs. The second set of lists will be used to run the CROG clustermap and boxplot.  

### Summary of OG classification

The following steps were aimed to create lists of orthogroups that each genome has genes assigned to.  

To run the OG list commands in batch, first create a list with all the annotation names:
```bash
ls | grep "classification" | sed 's/.orthogroup.*//' > annotation.list
```

Now let's create the OG lists for all the annotation:
```bash
for i in $(cat annotation.list); do cut -f2 $i.orthogroup.classification | sort | uniq > $i.orthogroup.list; done
```

Count how many OGs in total contains genes from at least one of the investigated *M. domestica* annotations
```bash
cat *.orthogroup.list | sort | uniq | wc -l
#   11699. 11698 OGs since the input files have header line.
```
Count how many 'WA 38' gene containing OGs are identified:
```bash
wc -l WA-38.hapA.orthogroup.list
#   10495 WA-38.hapA.orthogroup.list. 10494 OGs contain genes from HapA since the file has a header
wc -l WA-38.hapB.orthogroup.list
#   10512 WA-38.hapB.orthogroup.list. 10511 OGs contain genes from HapB
```

Upset plot for analyzing and visualizing share and unique OGs among apple genomes is done using TBtools with the Malus domestica orthogroup lists as input. All the overlapping lists are saved in a directory named `Upset`.  

Additionally, count how many OGs are shared by both haplomes of 'WA 38' and another annotation
```bash
cat Gala.hap.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list| sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l
#    9772. 9771 OGs are shared since the all three input files have header.
cat GDDH13.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list| sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l 
#    9823. 9822 OGs are shared since the all three input files have header.
cat HanfuTH.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list| sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l 
#    9635. 9634 OGs are shared since the all three input files have header.
cat Honeycrisp.hapA.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list| sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l 
#   10087. 10086 OGs are shared since the all three input files have header.
cat Honeycrisp.hapB.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list| sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l
#   10118. 10117 OGs are shared since the all three input files have header.
cat Honeycrisp.hapA.orthogroup.list Honeycrisp.hapB.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list| sort | uniq -c | grep -E '(^|\s)4($|\s)' | wc -l
#    10017. 10016 OGs are shared by both haplomes from both WA 38 and Honeycrisp
cat Anton.hapA.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list | sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l
#    9946. 9945 OGs are shared since the all three input files have header.
cat Anton.hapB.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list | sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l
#    9960. 9959 OGs are shared since the all three input files have header.
cat Fuji.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list | sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l
#    9948. 9947 OGs are shared since the all three input files have header.
cat M19.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list | sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l
#    9820. 9819 OGs are shared since the all three input files have header.
cat MM106.orthogroup.list WA-38.hapA.orthogroup.list WA-38.hapB.orthogroup.list | sort | uniq -c | grep -E '(^|\s)3($|\s)' | wc -l
#    9827. 9826 OGs are shared since the all three input files have header.
```

### CoRe OrthoGroup (CROG) analysis
Next, we need to count how many genes are classified into each CROG from each annotation. Some annotations (i.e. WA 38, Honeycrisp, dAnjou, and Malpr) contain multiple splice variants from the same gene, we do not want to count the gene multiple times if all the splice variants are classified into the same orthogroup. But if some splice variants got classified into different orthogroups, then we do want to count that gene multiple times.  

First, create lists with orthogroups (if X number of genes are classified into the same orthogroup, then this OG ID will be kept X times in this list) and remove OG occurances if the the occurance is caused by splice variances classified into the same OG. In the WA 38, Honeycrisp and dAnjou annotations, splice variances were named with .t#, in the Malpr genome, splice variants were named with .1 .2 etc, therefore, different regular expression patterns are needed to create the orthogroup lists.  

First, let's work with those used .t1 .t2 etc to name splice variants 
```bash
echo "Honeycrisp.hapA\nHoneycrisp.hapB\nPyrco.da.hapA\nPyrco.da.hapB\nWA-38.hapA\nWA-38.hapB" > sv.type1.annotation.list
for i in $(cat sv.type1.annotation.list); do cut -f1,2 $i.orthogroup.classification | sed 's/\.t/\t/' | cut -f1,3 | sort | uniq | cut -f2 > $i.OG.list.rm.sv; done
```
Next, create the table for Malpr
```bash
cut -f1,2 Malpr.orthogroup.classification | sed 's/\./\t/' | cut -f1,3 | sort | uniq | cut -f2 > Malpr.OG.list.rm.sv
```
Last, create the tables for those that do not have splice variants
```bash
echo "Malba\nMalfu.hapA\nMalfu.hapB\nAnton.hapA\nAnton.hapB\nFuji\nM19\nMM106" > sv.type2.annotation.list
for i in $(cat sv.type2.annotation.list); do cut -f2 $i.orthogroup.classification > $i.OG.list.rm.sv; done
```
Next, count how many genes in each annotation are assigned to CROGs.  
Download the [supplemental tables](https://www.frontiersin.org/articles/10.3389/fpls.2022.1011199/full) from the [PlantTribes manuscript](https://www.frontiersin.org/articles/10.3389/fpls.2022.1011199/full) to get the CROG list.  
Extract column 1 from SupTable 8 into a seperate text file and remove 'OG' from the text - creating a file named 'CROGs.list'.  

Now, count how many occurances of each CROG has in each '.OG.list.rm.sv' file, which equals to how many genes were classified into each CROG from each annotation.  

```bash
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Honeycrisp.hapA.OG.list.rm.sv)" >> Honeycrisp.hapA.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Honeycrisp.hapB.OG.list.rm.sv)" >> Honeycrisp.hapB.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Malba.OG.list.rm.sv)" >> Malba.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Malfu.hapA.OG.list.rm.sv)" >> Malfu.hapA.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Malfu.hapB.OG.list.rm.sv)" >> Malfu.hapB.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Malpr.OG.list.rm.sv)" >> Malpr.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Pyrco.da.hapA.OG.list.rm.sv)" >> Pyrco.da.hapA.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Pyrco.da.hapB.OG.list.rm.sv)" >> Pyrco.da.hapB.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" WA-38.hapA.OG.list.rm.sv)" >> WA-38.hapA.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" WA-38.hapB.OG.list.rm.sv)" >> WA-38.hapB.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Anton.hapA.OG.list.rm.sv)" >> Anton.hapA.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Anton.hapB.OG.list.rm.sv)" >> Anton.hapB.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" Fuji.OG.list.rm.sv)" >> Fuji.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" M19.OG.list.rm.sv)" >> M19.CROG.gene.count.csv; done
for i in $(cat CROGs.list); do echo "$i,""$(grep -c "^$i$" MM106.OG.list.rm.sv)" >> MM106.CROG.gene.count.csv; done
```

CROG gene counts from previously published genomes were extracted from SupTable 9 of the PlantTribes2 manuscript supplemental table.  Six rencently published high quality annotations (*Malus domestica* 'Gala', *M. domestica* 'GDDH13', *M. sieversii*, *M. sylvestris*, *Pyrus pyrifolia* 'Nijisseiki', and *P. betulifolia*) were selected for CROG analysis with the annotation from above.  

Since the SupTable 9 is already in Microsoft Excel, we will create the new CROG count file in Excel as well.  

First, CROG gene counts of the 6 published annotations were extracted from SupTable 9. Next, the newly created counts from each annotation were added as new columns. Lastly, corresponding genome identifier was added in the first row as headers. This CROG count table was saved as 'CROG.gene.count.summary.csv'.

A z-score normalized gene count table was created in excel (the calculations are performed in the file named 'zscore.cal.xlsx') and saved as 'CROG.gene.count.zscore.csv'

Create the clustermap and box plot as described in 'Clustermap_boxplot.ipynb'.
