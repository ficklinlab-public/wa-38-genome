Create sym links to the CDS and protein files from the annotation
```bash
ln -s ../../../../05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa 
ln -s ../../../../05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.CDS.fna
```
Run PlantTribes2 geneFamilyClassifier with the 26Gv2.0 database
```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```
