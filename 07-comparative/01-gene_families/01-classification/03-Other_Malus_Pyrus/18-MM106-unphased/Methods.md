Protein sequences were downloaded from GDR
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/mm106_cau_v1.0.a2/genes/MM106_unphased.pep.fa.gz
gunzip MM106_unphased.pep.fa.gz
```

Since we are not doing any phylogenetic analysis, we will only run the classifier on the protein sequences.

```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```

