Protein sequences were downloaded from GDR
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica_HFTH1_v1.0.a1/genes/HFTH1.gene.pep.fa.gz
gunzip HFTH1.gene.pep.fa.gz
```

Since we are not doing any phylogenetic analysis, we will only run the classifier on the protein sequences.

```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```

