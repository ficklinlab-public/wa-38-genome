Protein sequences were downloaded from GDR
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_sylvestris/Msylvestris_haploid_v1/genes/Msylvestris_haploid_v2.pep.fa.gz
gunzip Msylvestris_haploid_v2.pep.fa.gz
```

Since we are not doing any phylogenetic analysis, we will only run the classifier on the protein sequences.

```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```

