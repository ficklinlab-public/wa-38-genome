Protein sequences were downloaded from GDR
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_sieversii/Msieversii_haploid_v1/genes/Msieversii_haploid_v2.pep.fa.gz
gunzip Msieversii_haploid_v2.pep.fa.gz
```

Since we are not doing any phylogenetic analysis, we will only run the classifier on the protein sequences.

```bash
sbatch 01-PlantTribes_orthogroup_classifier.srun
```

