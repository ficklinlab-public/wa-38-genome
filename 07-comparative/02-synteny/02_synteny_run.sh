#!/bin/bash
#SBATCH --partition=ficklin_class
#SBATCH --account=ficklin_class
#SBATCH --job-name=Synteny-hapA
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=itsuhiro.ko@wsu.edu
#SBATCH --time=7-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=5
#SBATCH --mem=30GB

module use /scidas/research/modulefiles
module add singularity 

singularity exec --no-home -B ${PWD}  docker://systemsgenetics/actg-wgaa-genespace:1.2.3 \
	Rscript synteny_run.R