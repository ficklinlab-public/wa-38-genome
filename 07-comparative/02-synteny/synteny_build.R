library(GENESPACE)
parsedPaths <- parse_annotations(
  rawGenomeRepo = ".", 
  genomeDirs = "WA_38_hapA",
  genomeIDs = "WA_38_hapA",
  gffString = "gff3",
  faString = "faa",
  headerEntryIndex = 1, 
  gffIdColumn = "ID",
  genespaceWd = ".")

parsedPaths <- parse_annotations(
  rawGenomeRepo = ".", 
  genomeDirs = "WA_38_hapB",
  genomeIDs = "WA_38_hapB",
  gffString = "gff3",
  faString = "faa",
  headerEntryIndex = 1, 
  gffIdColumn = "ID",
  genespaceWd = ".")

parsedPaths <- parse_annotations(
  rawGenomeRepo = ".", 
  genomeDirs = "HC_hapA",
  genomeIDs = "HC_hapA",
  gffString = "gff3",
  faString = "fa",
  headerEntryIndex = 1, 
  gffIdColumn = "transcript_id",
  genespaceWd = ".")

parsedPaths <- parse_annotations(
  rawGenomeRepo = ".", 
  genomeDirs = "HC_hapB",
  genomeIDs = "HC_hapB",
  gffString = "gff3",
  faString = "fa",
  headerEntryIndex = 1, 
  gffIdColumn = "transcript_id",
  genespaceWd = ".")

parsedPaths <- parse_annotations(
  rawGenomeRepo = ".", 
  genomeDirs = "GDDH13",
  genomeIDs = "GDDH13",
  gffString = "gff3",
  faString = "fasta",
  headerEntryIndex = 1, 
  gffIdColumn = "Name",
  genespaceWd = ".")