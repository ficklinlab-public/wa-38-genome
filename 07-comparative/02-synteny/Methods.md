Synteny analysis allows us to study and visualize similarities and differences in structures among selected genomes by identifying homologous genes and gene orders. It is another important comparative analysis in genome studies. Here we will compare the two 'WA 38' haplomes, the two haplomes from 'Honeycrisp', and the 'GDDH13' genome. The hypotheses are: 1) for the most part, those five genomes/ haplomes share the same genes and gene orders; 2) 'WA 38' hapA is the most simialr to 'Honeycrisp' as it is derived from 'Honeycrisp'; 3) chromosome inverions observed from mummer alignment may affect gene orders in the affected regions.   

### Prepare the genomes
Create symlink to 'WA 38' protein files and GFF3 files. Note: GeneSpace does not require the assembly.  
```bash
ln -s ../../05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa
ln -s ../../05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 WA_38_hapA.gff3
ln -s ../../05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa
ln -s ../../05-structural_annotation/02-annotate-genes/08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3 WA_38_hapB.gff3
```

Download protein and GFF3 files for 'Honeycrips' and 'GDDH13'
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/g.Honeycrisp_HAP2_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
gunzip g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz
gunzip g.Honeycrisp_HAP2_braker1+2_combined_fullSupport_longname_filtered.pep.fa.gz

wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/Honeycrisp_HAP1_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/genes/Honeycrisp_HAP2_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz
gunzip Honeycrisp_HAP1_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz
gunzip Honeycrisp_HAP2_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt.gz

wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/genes/GDDH13_1-1_prot.fasta.gz
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/genes/gene_models_20170612.gff3.gz
gunzip GDDH13_1-1_prot.fasta.gz
gunzip gene_models_20170612.gff3.gz
```

Because 'WA 38' and 'Honeycrisp' annotations contain multiple isoforms, using all of them will inflate gene count, therefore, only the longest isoforms were isolated from the protein files. No need to edit the GFF3 files as GeneSpace will ignore features that don't match a protein from the protein file.  

We will be using a modified version of the 'get_longest_isoform_seq_per_trinity_gene.pl' script downloaded from 'trinityrnaseq' GitHub repository at: https://github.com/trinityrnaseq/trinityrnaseq/blob/master/util/misc/get_longest_isoform_seq_per_trinity_gene.pl. The modification that was made from the original script is in line 29. The regular expression in that line was modified to match the names of our gene models. The modified script is named as `longestIsoform2.pl`.  

The Fasta_reader package is required for running this isoform script and this package is downloaded from: https://github.com/genome-vendor/trinity/blob/master/PerlLib/Fasta_reader.pm  

```bash
perl longestIsoform2.pl Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa > WA_38_hapA_longest.faa
perl longestIsoform2.pl Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa > WA_38_hapB_longest.faa
perl longestIsoform2.pl g.Honeycrisp_HAP1_braker1+2_combined_fullSupport_longname_filtered.pep.fa > HC_hapA_longest.faa
perl longestIsoform2.pl g.Honeycrisp_HAP2_braker1+2_combined_fullSupport_longname_filtered.pep.fa > HC_hapB_longest.faa
```

To run GeneSpace, each genome needs to be in their own folders and the name of those folders will show up on the final synteny plots. Therefore, let's now create those folders and move the protein and GFF3 files into the corresponding folders.  
```bash
mkdir WA_38_hapA
mkdir WA_38_hapB
mkdir HC_hapA
mkdir HC_hapB
mkdir GDDH13
```

Now move the files
```bash
mv WA_38_hapA_longest.faa WA_38_hapA.gff3 WA_38_hapA
mv WA_38_hapB_longest.faa WA_38_hapB.gff3 WA_38_hapB
mv HC_hapA_longest.faa Honeycrisp_HAP1_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt HC_hapA
mv HC_hapB_longest.faa Honeycrisp_HAP2_braker1+2_combined_fullSupport_renamed_filtered_testNoStartNoStop_added_long_short_names.gff3.txt HC_hapB
mv GDDH13_1-1_prot.fasta gene_models_20170612.gff3 GDDH13
```

### Reformat files for GeneSpace

GeneSpace require certain file format and provided functions to create those files.  
```bash
sbatch 01_synteny_build.sh
```

### Create synteny plots

Now we have all the required files in the correct format, we can create the final plots.
```bash
sbatch 02_synteny_run.sh
```
