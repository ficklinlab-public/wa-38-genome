Directory Overview
==================
This directory contains steps to assemble 'WA 38' genome using both PacBio long reads and Omni-C reads. The assembly produced by hifiasm is then scaffolded with the Omni-C reads using YaHS. The scaffolds were then manually examined for errors.  

Authors: Huiting Zhang  
Last Updated: Aug 3rd 2024  

Directory Structure
===================
The following describes the purpose for each directory:
- 01-hifiasm  
Hifiasm is used to create the phased assembly at contig level.  

- 02-bwa_hic_alignment  
Omni-C read were alignemnt to assembled contigs with bwa. The alignment is required for both quality check and scaffolding.  

- 03-hic_qc_quality_check  
Examine the quality of the Hi-C data  

- 04-mummer_contigs  
This is a sanity check step where contigs from the two haplome assemblies were aligned to each other.  

- 05-yahs_scaffolding  
Scaffolding step using Omni-C reads and the assembled contigs.  

- 06-hic_maps  
Hi-C/ Omni-C maps are created with YaHS.  

- 07-juicebox_curation  
Hi-C / Omni-C maps were loaded to juicebox for manual curation. The final scaffold FASTA files were created.  

- 08-original_assembly_busco  
BUSCO analysis of the scaffold assemblies.  

- 09-chromosome-rename  
Chromosomes were renamed according to that of 'Gala' hapA assembly.  

- 10-chromosome-reorient  
Chromosomes were reoriented to match 'Gala' hapA assembly.

- 11-filtering  
Contaminants (i.e. plastids, bacterial, and virus) were identified and removed. A BUSCO analysis was performed on the filtered assemblies.  

- 12-kmer-phasing  
Meryl was used to identify parentage of the chromosomes. Chromosomes from the same parents were placed in the same haplome. The 2 haplomes are now designated as hapA (with a Honeycrisp origin) and hapB (with a Enterprise origin)  

- 13-final_assembly_busco_mummer  
Last sanity check as well as structural variantion and BUSCO analysis.  