Since we have the Honeycrisp assembly, we can use Meryl to identify which 'WA 38' chromosomes are originated from Honeycrisp and the rest will have an Enterprise origin.  

### Obtain assemblies from 'WA 38' and 'Honeycrisp'  
First, create sym links to the assemblies
```bash
ln -s ../11-filtering/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa Mdom_CC_hapA.fasta
ln -s ../11-filtering/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa Mdom_CC_hapB.fasta
```

Next, download Honeycrisp assemblies from GDR
```bash
wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/assembly/Malus_x_domestica_Honeycrisp_HAP1_v1.1.a1.fasta.gz
gunzip Malus_x_domestica_Honeycrisp_HAP1_v1.1.a1.fasta.gz
mv Malus_x_domestica_Honeycrisp_HAP1_v1.1.a1.fasta Mdom_HC_hapA.fasta

wget https://www.rosaceae.org/rosaceae_downloads/Malus_x_domestica/md_Honeycrisp_v1.0/assembly/Malus_x_domestica_Honeycrisp_HAP2_v1.1.a1.fasta.gz
gunzip Malus_x_domestica_Honeycrisp_HAP2_v1.1.a1.fasta.gz
mv Malus_x_domestica_Honeycrisp_HAP2_v1.1.a1.fasta Mdom_HC_hapB.fasta
```

### Prepare input files for Meryl  
Now we want to create individual files for each 'WA 38' chromosome from each haplome. A custome script will be used.
```bash
for i in {1..17}; do
	for h in {"A","B"}; do
		perl subset_fa.pl -f Mdom_CC_hap${i}.fasta \
		-s chr${i}${h} -o Mdom_CC_chr${i}${h}.fasta
	done
done
```

We do the same for Honeycrisp assemlies
```bash
for i in {1..17}; do
	for h in {"A","B"}; do
		perl subset_fa.pl -f Mdom_HC_hap${i}.fasta \
		-s chr${i}${h} -o Mdom_HC_chr${i}${h}.fasta
	done
done
```

We don't need to seperate k-mers from the two haplomes from Honeycrisp, let's combine chromosomes from both haplomes and create one file.
```bash
for i in {1..17}; do
	cat Mdom_HC_chr${i}A.fasta Mdom_HC_chr${i}B.fasta > Mdom_HC_chr${i}Both.fasta
done
```

### Count K-mers  
Now that we have the chromosomes seperated, we can count k-mers for each of them.  

```bash
sbatch 01-meryl-count-CC.srun
sbatch 02-meryl-count-HC.srun
```

### k-mer comparison
Next we use meryl again to compare k-mers from each haplome of 'WA 38' to 'Honeycrisp'  
The 'difference' command return k-mers that occur in the first input, but none of the other inputs  
The 'intersect-sum' command return k-mers that occur in all inputs, set the count to the sum of the counts  
```bash
sbatch 03-meryl-intersect-hapA.srun
sbatch 04-meryl-intersect-hapB.srun
```

### summarize the results
