Input_file	Dataset	Complete	Single	Duplicated	Fragmented	Missing	n_markers	Scaffold N50	Contigs N50	Percent gaps	Number of scaffolds
Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa		98.7	59.7	39.0	0.5	0.8	2326	37154807	35835625	0.000%	17
Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa		98.7	58.9	39.8	0.5	0.8	2326	36115806	35315177	0.000%	17
