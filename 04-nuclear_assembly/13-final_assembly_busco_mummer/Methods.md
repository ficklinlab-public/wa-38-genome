## BUSCO  
Before we run BUSCO we need access to the final scaffold FASTA file. 
We will create sym links to files we saved after Juicebox curation. BUSCO
can process each of our haplotypes at the same time if we put them in 
the same directory, so the following code creates a new directory
named `busco_input_files` and creates sym links there.

```bash
mkdir busco_input_files
cd busco_input_files
ln -s ../../12-kmer-phasing/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
ln -s ../../12-kmer-phasing/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
cd ..

```

Now run BUSCO on these FASTA files

```bash
sbatch 01-busco.srun
```

## Mummer

Use MUMmer to align 'WA 38' fully haplotype-resolved assemblies (i.e. hapA is from Honeycrisp; hapB from Enterprise)  

Run MUMmer alignment
```bash
sbatch 01-mummer.srun
```

Compress the delta files and uploadeto [Assemblytics](http://assemblytics.com/) to generate summary files.  
```bash
gzip WA_38_phased_haps.delta
```

The result from Assemblitics is here:
--http://assemblytics.com/analysis.php?code=m31sXx31pb04o4RoNegb  

A copy of the result is downloaded locally and saved as `WA_38_phased_chrs.Assemblytics_results.zip`  
