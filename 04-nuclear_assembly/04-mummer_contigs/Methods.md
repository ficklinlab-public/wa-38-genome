To calculate the stats of our hifiasm assembled haplotypes, here we use MUMmer and Assemblytics.

### MUMmer

Create symk links to our assembly haplotypes
```bash
ln -s ../01-hifiasm/WA_38.asm.hic.hap1.p_ctg.fa
ln -s ../01-hifiasm/WA_38.asm.hic.hap2.p_ctg.fa
```

Run mummer
```bash
sbatch 01-mummer.srun
```

Once completed, gzip (compress) the delta file:
```
gzip out.delta
```

Next upload, the compressed delta file to the [Assemblytics website](http://assemblytics.com/) for analysis.

The results from Assemblytics are here:                  
--http://assemblytics.com/analysis.php?code=qQwW0bR4MJ29avfFAzZQ  

### MUMmer dandiff

In addition, I also tried the fucntion in MUMmer package called dnadiff

You can also run it to get the haplotype info which require the delta file and the assembled fasta file

```bash
sbatch 02-dandiff_mummer.srun
```

The result is in `out.report` file in the same folder.
