Directory Overview
==================
This diresctory includes steps for sequence quality accessment and genome complexity analyses.  
Authors: Huiting Zhang  
Last updated: Jan. 3rd 2024  

Directory Structure
===================
The following describes the purpose for each directory:  
- 01-FastQC-PacBio  
- 02-fastp_shotgun_DNA  
- 03-FASTQC_shotgun_DNA  
- 04-FASTQC_trimmed_shotgun_DNA  
- 05-complexity_analysis  
    - 01-jellyfish_trimmed_shotgun  
    - 02-genomescope_analysis  
- 06-MultiQC  
- 07-FASTQC_Hi-C  