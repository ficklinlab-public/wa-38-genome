*This step was done after finishing `04-nuclear_assembly/01-hifiasm`*  

We will use the hifiasm assemblies from both haplomes as input to run MitoHifi. Let's first combine the contigs from both Hap1 and Hap2.  
```bash
ln -s ../../../04-nuclear_assembly/01-hifiasm/WA_38.asm.hic.hap1.p_ctg.fa
ln -s ../../../04-nuclear_assembly/01-hifiasm/WA_38.asm.hic.hap2.p_ctg.fa
cat *.p_ctg.fa > WA_38.asm.hic.haps.merged.fa
``` 

MitoHifi requires a reference mitochndria assembly and annotation. The Malus domestica mitochondria sequencing from NCBI (NC_018554.1) wad downloaded in both FASTA format and GenBank format, and saved as `Malusxdomestica_mito.fasta` and `Malusxdomestica_mito.gb`, respectively.  

Run MitoHifi
```bash
sbatch 01-mito_hifi_contigs.srun
```