Our assembled chromosomes certainly contains telomeres as we can see large repeat regions at the ends of the chromosomes. Therefore, we will alao annotate the telomere sequences. We can compare our results to this [BioRxiv preprint](https://www.biorxiv.org/content/10.1101/2024.03.27.586973v1.full) to see if telomeres in WA 38 genome has the expected length and combination.  

If you have not yet, create a conda environment and install the software first  
```bash
conda create -n t_to_t
conda install -c bioconda tidk
```

Create symlinks to the assemblies
```bash
ln -s ../../../04-nuclear_assembly/12-kmer-phasing/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
ln -s ../../../04-nuclear_assembly/12-kmer-phasing/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa
```

Run telomere annotation
```bash
sbatch 01_telomere.sh
```