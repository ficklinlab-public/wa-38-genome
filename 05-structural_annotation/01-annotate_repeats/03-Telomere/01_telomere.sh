#!/bin/bash
#SBATCH --partition=kamiak
#SBATCH --job-name=tidk_wa38
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=itsuhiro.ko@wsu.edu
#SBATCH --time=7-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=5
#SBATCH --mem=10GB

module add conda 

conda activate t_to_t

tidk explore --fasta Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa --output wa38hapA --dir wa38_telomere --minimum 2 --maximum 20

tidk explore --fasta Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa --output wa38hapB --dir wa38_telomere --minimum 2 --maximum 20
