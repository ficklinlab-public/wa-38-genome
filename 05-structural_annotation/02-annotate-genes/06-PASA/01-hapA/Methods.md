Setup
=====
For this step we are using RNA-seq as input. Online instructions for ths step can be found here:  
https://github.com/PASApipeline/PASApipeline/wiki/PASA_RNAseq.  

First, create sym links to the genome assembly. Since the scaffolds were removed as being 
plastid sequence or bacterial contaminant we will use the filtered assembly file which
only contains the chromosomal sequences. The previous gene filtering step also removed
gene models from the scaffolds, so this assembly should be sufficient.

```bash
ln -s ../../../../04-nuclear_assembly/12-kmer-phasing/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa
```

Link to the gene models from TSEBRA. We must give it a prefix of `.gff3` or PASA will not use it.
```bash
ln -s ../../04-TSEBRA/01-hapA/braker_combined_renamed.gff braker_combined_renamed.hapA.gff3
```

Link to the transcript sequences derived from Trinity. We will link to the referenced based
assembly that we created in the previous step and to the de novo assembly that was performed
prior to our assembly project.

```bash
ln -s ../../05-Trinity/01-hapA/trinity_out_dir/Trinity-GG.fasta
ln -s ../../../../01-input_data/TRINITY_assembly/transcripts/WA_38_combined.Trinity.fasta Trinity-denovo.fasta
```

Next, combine both the de novo and reference guided assemblies into a single file:
```bash
cat Trinity-GG.fasta Trinity-denovo.fasta > Trinity-combined.fasta
```

Next, we need to perform a few tasks using the PASA software and setup of the MySQL 
databse. We will build a file containing the accessions of the de-novo assembly
used in step 2 and we will check the GFF file to make sure it is okay for use by
PASA.

```bash
sbatch 01-setup.srun
```
After this runs a new `tdn.accs` file should be created containing the accessions of the 
de novo transcripts and a new `mysql-files` folder where the MySQL data files will live. The
output files should indicate there are no problems with the GFF file.

Step 1:  Alignment Assembly Pipeline
====================================
Here we will align the de novo assembly to the genome reference. Trinity requires a 
configuration file for the alignment step.  This is found in the file named `alignAssembly.config`.  

Now run the assembly step. This will call the script named: `PASA_assembly.sh`

```bash
sbatch 02-PASA_assembly.srun
```

Step 2: Build a Transcriptome Database
======================================
Now that the de novo assembly reads have been aligned to the refernece genome
we can build a comprehensive transcriptome database by giving PASA the FASTA file
containing both the de novo and reference guided assemblies.

```bash
sbatch 03-PASA_build_transdb.srun
```

Step 3: Annotate Round 1
========================
PASA will compare the PASA alignment assemblies to existing gene models to identify 
where updates can be automatically performed to gene structures in order to 
incorporate the transcript alignments. This step requires a configuration
file named `annotCompare.config`.

```bash
sbatch 04-PASA_annotate1.srun
```

Steps 4-7: Annotate Round 2-4
==============================
We will rerun the PASA annotation step but using the output from the previous step. This
is supposed to improve the models further.

```bash
sbatch 05-PASA_annotate2.srun
```

Step 5: Alternate Splicing
==========================
This last step will make PASA compare its clustered transcripts to generate a report about splice variants.  
```bash
sbatch 08-PASA_alt_splice.srun
```

Step 6: Viewing via PASAWeb
===========================
PASA provides a tool for viewing the changes it makes to the gene models. This is a
a web-based tool. To view this report while running on a cluster like Kamiak we 
need to first launch the tool then use SSH port forwarding to be able to view it.
To launch the tool use the script:

```bash
sbatch 09-PASA_web.sun
```

After it is running you will need to find the IP address of the node on which the job
is running. On Kamiak this can be done on the login node with the following command:

```bash
nslookup <node>
```

Where `<node>` should be replaced with the name of the node on which the job is running. The
command should give you the IP address of the node.

Now, you can use SSH to connect to the cluster and tunnel a connection to a port on your local
machine to a port on the cluster node. The following SSH command should be adapted and used:

```bash
ssh -N <user>@kamiak.wsu.edu -L 8080:<node ip>:8080 
```
Sustitute the following:

- <user>: your username on the cluster
- kamiak.wsu.edu: useful for Kamiak but change to whatever cluster is being used
- <node ip>:  Use the IP address of the node on which the job is running on the cluster

The port `8080` is the one used for PASAweb.

You can now look at PASAweb by opening a web browser and using this web address:  http://localhost:8080 (http://127.0.0.1:8080 should work as well).
