#!/bin/bash

# Start the MySQL Server
export MYSQL_UNIX_PORT=/tmp/mysqld.${SLURM_TASK_PID}.sock
mysqld_safe --datadir=$PWD/mysql-files --log-error=$PWD/mysql-files/error.log &

# Wait to get going until MySQL is fully running
while ! mysqladmin ping -h "localhost" -u root --silent; do
  echo "Waiting on MySQL..."
  sleep 10
done

# Setup the PASA config file so PASA knows how to talk to mysql. 
export MYSQL_UNIX_PORT=/tmp/mysqld.${SLURM_TASK_PID}.sock
socket_path=$(echo $MYSQL_UNIX_PORT | perl -p -e 's/\//\\\//g')
cp pasa.config.template pasa.config
perl -pi -e "s/__SOCKET__/$socket_path/" pasa.config
export PASACONF=$PWD/pasa.config

# Run PASA
Launch_PASA_pipeline.pl \
  -c alignAssembly.config \
  -C \
  -R \
  -g Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa \
  -t Trinity-denovo.fasta \
  --TDN tdn.accs \
  --ALIGNERS blat,gmap \
  --CPU 20

# Cleanly shutdown MySQL
mysql -u root -e  "SHUTDOWN;"

sleep 30
