# Create a file containing the list of transcript accessions that correspond to the 
# Trinity de novo assembly (full de novo, not genome-guided).
$PASAHOME/misc_utilities/accession_extractor.pl < Trinity-denovo.fasta > tdn.accs

# As a sanity check, make sure the GFF fiel is okay:
$PASAHOME/misc_utilities/pasa_gff3_validator.pl braker_combined_renamed.hapA.gff3

# Create and initialize the MySQL database
mkdir -p mysql-files
chmod 750 mysql-files

export MYSQL_UNIX_PORT=/tmp/mysqld.${SLURM_TASK_PID}.sock
mysqld --initialize-insecure --datadir=$PWD/mysql-files --log-error=$PWD/mysql-files/error.log 

# Start the MySQL Server
mysqld_safe --datadir=$PWD/mysql-files --log-error=$PWD/mysql-files/error.log &

# Wait to get going until MySQL is fully running
while ! mysqladmin ping -h "localhost" -u root --silent; do
  echo "Waiting on MySQL..."
  sleep 10
done

# Create the pasa_full and pasa_access users as expected by the Docker image's PASAweb configuration.
mysql -u root -e "CREATE USER IF NOT EXISTS 'pasa_access'@'localhost' IDENTIFIED BY 'pasa_access';"
mysql -u root -e "GRANT SELECT ON *.* TO 'pasa_access'@'localhost';"

mysql -u root -e "CREATE USER IF NOT EXISTS 'pasa_full'@'localhost' IDENTIFIED BY 'pasa_full';"
mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'pasa_full'@'localhost';"

# Cleanly shutdown MySQL
mysql -u root -e  "SHUTDOWN;"


