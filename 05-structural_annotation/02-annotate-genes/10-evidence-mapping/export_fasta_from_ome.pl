#!/usr/bin/perl -w
use strict;
my %seq;
my $id = "";
my $contig="";
my $ome_fasta = $ARGV[0] || die "need transcriptome fast file\n";
my $contig_file = $ARGV[1] || die "need contig file\n";
open IN, "<$ome_fasta";
while (<IN>) {
    chomp;
    if (/^>(\S+)/){
    $id = $1;
    }
    else {
    $seq{$id} .= $_; #print "$id\t$seq{$id}\n";
    }
}
close IN;
#print "$id\n$seq{$id}\n";

open IN2, "<$contig_file";
while (<IN2>) {
chomp;
$contig = $_; 
print ">$contig\n"."$seq{$contig}\n";
}
close IN2;