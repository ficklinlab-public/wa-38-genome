#!/bin/bash
#SBATCH --partition=ficklin
#SBATCH --job-name=WA38_subset_BUSCO
#SBATCH --output=%x_%j.out
#SBATCH --error=%x_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=huiting.zhang@wsu.edu
#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=8GB

module add singularity

# Remove any previous run of busco
rm -rf WA38_subset_busco

singularity exec -B ${PWD} docker://ezlabgva/busco:v5.4.3_cv1 \
  busco \
    -i Subsets_4_BUSCO \
    -m proteins \
    -o WA38_subset_busco \
    -l eudicots_odb10 \
    -c 16