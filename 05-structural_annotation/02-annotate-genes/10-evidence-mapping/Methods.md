### Gather support evidence
Create symlinks to EnTAP results
```bash
ln -s ../../../06-functional_annotation/01-EnTAP/01-hapA/results/EnTAP/outfiles/final_results/final_annotated.faa hapA.final_annotated.faa
ln -s ../../../06-functional_annotation/01-EnTAP/02-hapB/results/EnTAP/outfiles/final_results/final_annotated.faa hapB.final_annotated.faa
```
Extract gene IDs from the faa files
```bash
grep ">" hapA.final_annotated.faa | sed s'/>//' > hapA.entap.txt
grep ">" hapB.final_annotated.faa | sed s'/>//' > hapB.entap.txt
```

Create sym link to the PlantTribes2 results
```bash 
ln -s ../../../07-comparative/01-gene_families/01-classification/01-hapA/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary hapA.proteins.both.26Gv2.0.bestOrthos.summary
ln -s ../../../07-comparative/01-gene_families/01-classification/02-hapB/geneFamilyClassification_dir/proteins.both.26Gv2.0.bestOrthos.summary hapB.proteins.both.26Gv2.0.bestOrthos.summary
```
Extract gene IDs from the summary files
```bash
cut -f1 HapA.proteins.both.26Gv2.0.bestOrthos.summary | grep "Maldo" > hapA.gene.family.txt
cut -f1 HapB.proteins.both.26Gv2.0.bestOrthos.summary | grep "Maldo" > hapB.gene.family.txt
```

Next, we run the `selectSupportedSubsets.py` script provided by the BRAKER group to select genes with evidence. Scripts are downloaded from [BRAKER GitHub](https://github.com/Gaius-Augustus/BRAKER/tree/report/scripts/predictionAnalysis).  
We first need to create symlinks to the prediction files and hints files.  
HapA RNA-seq evidence  
```bash
ln -s ../02-Braker1_RNAseq/01-hapA/braker/augustus.hints.gtf hapA.RNA.hints.gtf
ln -s ../02-Braker1_RNAseq/01-hapA/braker/hintsfile.gff hapA.RNA.hintsfile.gff
```
HapB RNA-seq evidence  
```bash
ln -s ../02-Braker1_RNAseq/02-hapB/braker/augustus.hints.gtf hapB.RNA.hints.gtf
ln -s ../02-Braker1_RNAseq/02-hapB/braker/hintsfile.gff hapB.RNA.hintsfile.gff
```
HapA protein evidence  
```bash
ln -s ../03-Braker2_protein/01-hapA/braker/augustus.hints.gtf hapA.prot.hints.gtf
ln -s ../03-Braker2_protein/01-hapA/braker/hintsfile.gff hapA.prot.hintsfile.gff
```
HapB protein evidence  
```bash
ln -s ../03-Braker2_protein/02-hapB/braker/augustus.hints.gtf hapB.prot.hints.gtf
ln -s ../03-Braker2_protein/02-hapA/braker/hintsfile.gff hapB.prot.hintsfile.gff
```

Create RNA-seq evidence files
```bash
python selectSupportedSubsets.py --fullSupport FULL_support_RNA_hapA --anySupport ANY_support_RNA_hapA --noSupport NO_support_RNA_hapA hapA.RNA.hints.gtf hapA.RNA.hintsfile.gff
python selectSupportedSubsets.py --fullSupport FULL_support_RNA_hapB --anySupport ANY_support_RNA_hapB --noSupport NO_support_RNA_hapB hapB.RNA.hints.gtf hapB.RNA.hintsfile.gff
```

Create protein evidence files
```bash
python selectSupportedSubsets.py --fullSupport FULL_support_prot_hapA --anySupport ANY_support_prot_hapA --noSupport NO_support_prot_hapA hapA.prot.hints.gtf hapA.prot.hintsfile.gff
python selectSupportedSubsets.py --fullSupport FULL_support_prot_hapB --anySupport ANY_support_prot_hapB --noSupport NO_support_prot_hapB hapB.prot.hints.gtf hapB.prot.hintsfile.gff
```

Each gene has multiple lines in the GFF file, and genes are seperated by "###". This makes downstream intersection analysis a challenge. Let's simplify that into one line (whichever line appears first in a gene) per gene.  
```bash
#HapA
grep "###" -A 1 FULL_support_prot_hapA | grep "chr" > FULL_support_prot_hapA_clean
grep "###" -A 1 ANY_support_prot_hapA | grep "chr" > ANY_support_prot_hapA_clean
grep "###" -A 1 FULL_support_RNA_hapA | grep "chr" > FULL_support_RNA_hapA_clean
grep "###" -A 1 ANY_support_RNA_hapA | grep "chr" > ANY_support_RNA_hapA_clean
#HapB
grep "###" -A 1 FULL_support_prot_hapB | grep "chr" > FULL_support_prot_hapB_clean
grep "###" -A 1 ANY_support_prot_hapB | grep "chr" > ANY_support_prot_hapB_clean
grep "###" -A 1 FULL_support_RNA_hapB | grep "chr" > FULL_support_RNA_hapB_clean
grep "###" -A 1 ANY_support_RNA_hapB | grep "chr" > ANY_support_RNA_hapB_clean
```

Now create a simplified version of the final annotation GFF3 file as well. Because we have renamed all the genes, we can not do a simple comparison, therefore, we will use bedtools's intersect function to assign evidence to the final gene models.  
```bash
ln -s ../08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3
ln -s ../08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3
grep "mRNA" Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 > wa38_mrna_hapA.gff3
grep "mRNA" Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3 > wa38_mrna_hapB.gff3
perl -p -e 's/ID=(Maldo.*?\.t\d+);.*$/$1-/' wa38_mrna_hapA.gff3 > wa38_mrna_clean_hapA_gff3
perl -p -e 's/ID=(Maldo.*?\.t\d+);.*$/$1-/' wa38_mrna_hapB.gff3 > wa38_mrna_clean_hapB_gff3
```

Finally, we will use bedtool to find the intersect and the resulting file will be cleaned up to create a table with genes and their corresponding evidence from RNA and homologous proteins.  
```bash
#HapA
bedtools intersect -a wa38_mrna_clean_hapA_gff3 -b FULL_support_prot_hapA_clean -c | bedtools intersect -a stdin -b ANY_support_prot_hapA_clean -c | bedtools intersect -a stdin -b FULL_support_RNA_hapA_clean -c | intersect -a stdin -b ANY_support_RNA_hapA_clean -c > evidence_hapA

perl -p -e 's/(Maldo.*?\.t\d+)\-(\d)(\d+)(\d\t\d+)$/$1\t$2\t$3\t$4/' evidence_hapA > evidence_hapA_clean.txt

cut -f 9,10,11,12,13 evidence_hapA_clean.txt RNAprot_table_hapA.txt

#HapB
bedtools intersect -a wa38_mrna_clean_hapB_gff3 -b FULL_support_prot_hapB_clean -c | bedtools intersect -a stdin -b ANY_support_prot_hapB_clean -c | bedtools intersect -a stdin -b FULL_support_RNA_hapB_clean -c | intersect -a stdin -b ANY_support_RNA_hapB_clean -c > evidence_hapB

perl -p -e 's/(Maldo.*?\.t\d+)\-(\d)(\d+)(\d\t\d+)$/$1\t$2\t$3\t$4/' evidence_hapB > evidence_hapB_clean.txt

cut -f 9,10,11,12,13 evidence_hapB_clean.txt RNAprot_table_hapB.txt
```

The gene family and EnTAP evidence were integrated into the RNA and protein table using Python scripts, detailed in the Integrate.ipynb notebook.  

### Create gene subsets based on evidence
Using the filter function in excel, genes with no evidence from both RNA and protein input were identified. Those with no evidence from the Full support columns were saved in `hapA.Full.No.evidence.ID.txt`, while those with no evidence from the Any support column were saved in `hapA.Any.No.evidence.ID.txt`.  

All the gene IDs were extratced and saved as `hapA.all.ID.txt`.  

Now Let's get the IDs for those that have evidence from at least one source, i.e. RNA or protein.  

First let's get those IDs for hapA.  
```bash
cat hapA.Any.No.evidence.ID.txt| sort | uniq > hapA.Any.No.evidence.ID.sort.txt
cat hapA.Full.No.evidence.ID.txt| sort | uniq > hapA.Full.No.evidence.ID.sort.txt
cat hapA.all.ID.txt| sort | uniq > hapA.all.ID.sorted.txt
comm -2 -3 hapA.all.ID.sorted.txt hapA.Any.No.evidence.ID.sort.txt > hapA.eitherAnySupport.ID.txt
comm -23 hapA.all.ID.sorted.txt hapA.Full.No.evidence.ID.sort.txt > hapA.eitherFullSupport.ID.txt
```
Same for hapB  
```bash
cat hapB.Any.No.evidence.ID.txt| sort | uniq > hapB.Any.No.evidence.ID.sort.txt
cat hapB.Full.No.evidence.ID.txt| sort | uniq > hapB.Full.No.evidence.ID.sort.txt
cat hapB.all.ID.txt| sort | uniq > hapB.all.ID.sorted.txt
comm -2 -3 hapB.all.ID.sorted.txt hapB.Any.No.evidence.ID.sort.txt > hapB.eitherAnySupport.ID.txt
comm -23 hapB.all.ID.sorted.txt hapB.Full.No.evidence.ID.sort.txt > hapB.eitherFullSupport.ID.txt
```

Now add in evidence from EnTAP and PlantTribes2  
Firts, using the filter function in excel, genes that has both EnTAP and PlantTribes 2 assignment identified. Those genes are saved in `hapA.Yes.EnTAP.PT2` and `hapB.Yes.EnTAP.PT2`.  

In addition, a list of genes with either EnTAP or PT2 assignment were identified.
```bash
cat cat hapA.entap.txt hapA.gene.family.txt| sort | uniq >  hapA.either.EnTap.PT2.txt
cat cat hapB.entap.txt hapB.gene.family.txt| sort | uniq >  hapB.either.EnTap.PT2.txt
```

Next, add genes with assignment to both EnTAP and PT2 into lists that were created based on RNA and protein evidences:
```bash
cat hapA.Yes.EnTAP.PT2.txt hapA.eitherFullSupport.ID.txt| sort | uniq > hapA.eitherFull.Yes.EnTAP.PT2.ID.txt
cat hapA.eitherFullSupport.ID.txt hapA.either.EnTap.PT2.txt | sort | uniq > hapA.eitherFull.either.EnTAP.PT2.ID.txt
cat hapB.Yes.EnTAP.PT2.txt hapB.eitherFullSupport.ID.txt| sort | uniq > hapB.eitherFull.Yes.EnTAP.PT2.ID.txt
cat hapB.eitherFullSupport.ID.txt hapB.either.EnTap.PT2.txt | sort | uniq > hapB.eitherFull.either.EnTAP.PT2.ID.txt
```

That's all the combinations we want, how many genes are there in each of those subsets?  
```bash
for i in $(ls *.ID.txt); do echo "${i}" && cat $i | sed 's/.t/\tt/' | cut -f1 | sort | uniq | wc -l; done
#hapA.eitherAnySupport.ID.txt
#   49829
#hapA.eitherFull.Yes.EnTAP.PT2.ID.txt
#   49417
#hapA.eitherFull.either.EnTAP.PT2.ID.txt
#   50087
#hapA.eitherFullSupport.ID.txt
#   43079
#hapB.eitherAnySupport.ID.txt
#   50861
#hapB.eitherFull.Yes.EnTAP.PT2.ID.txt
#   50005
#hapB.eitherFull.either.EnTAP.PT2.ID.txt
#   50743
#hapB.eitherFullSupport.ID.txt
#   43590
```

What's the BUSCO scores for each of those subsets?  
Let's extract the sequences for those subsets.  
First, create symlinks to the protein files
```bash
ln -s ../08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa
ln -s ../08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa
```
Then, extract the sequences
```bash
ls *.ID.txt | grep "hapA" > hapA.subset.list
ls *.ID.txt | grep "hapB" > hapB.subset.list
for i in $(cat hapA.subset.list); do perl export_fasta_from_ome.pl Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa $i > $i.faa; done
for i in $(cat hapB.subset.list); do perl export_fasta_from_ome.pl Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa $i > $i.faa; done
```

Create a new directory and move all the .faa files in there to run BUSCO.  
```bash
mkdir Subsets_4_BUSCO
mv *.ID.txt.faa Subsets_4_BUSCO
sbatch 01-subset_BUSCO.srun
```
