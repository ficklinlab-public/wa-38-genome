# BUSCO version is: 5.4.3 
# The lineage dataset is: eudicots_odb10 (Creation date: 2024-01-08, number of genomes: 31, number of BUSCOs: 2326)
# Summarized benchmarking in BUSCO notation for file /scidas/instruction/HORT503-Fall2022/wa-38-wgaa-official_files/05-structural_annotation/06-annotate-genes/08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa
# BUSCO was run in mode: proteins

	***** Results: *****

	C:98.5%[S:45.4%,D:53.1%],F:0.6%,M:0.9%,n:2326	   
	2289	Complete BUSCOs (C)			   
	1055	Complete and single-copy BUSCOs (S)	   
	1234	Complete and duplicated BUSCOs (D)	   
	14	Fragmented BUSCOs (F)			   
	23	Missing BUSCOs (M)			   
	2326	Total BUSCO groups searched		   

Dependencies and versions:
	hmmsearch: 3.1
	busco: 5.4.3
