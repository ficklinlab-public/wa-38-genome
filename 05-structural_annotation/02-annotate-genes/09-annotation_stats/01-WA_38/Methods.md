Here we want to get some basic statistics about gene features in hapA and hapB. We have ~6200 more genes in hapB than hapA, which is totally unexpected and we want to know why. First of all, we want to 1) calculate the average and total protein length of all the proteins; 2) calculate the average and total protein length on each chromosome; 3) gene count in each chromosome; and compare the results between the 2 haps.  

Create sym links to the CDS and protein files from the annotation
```bash
ln -s ../../08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa
ln -s ../../08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa
```

### Count of protein length
First, we count the length of each protein using bioawk. Bioawk is already installed in 01-input_data.
```bash
../../../../01-input_data/bioawk/bioawk -c fastx '{ print $name, length($seq)}' Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa > hapA.protein.length.txt
../../../../01-input_data/bioawk/bioawk -c fastx '{ print $name, length($seq)}' Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa > hapB.protein.length.txt
```
Next, we can calculate the average protein length and total length using:
```bash
awk '{x+=$2}END{print x/NR}' hapA.protein.length.txt
# 361.265
awk '{x+=$2}END{print x}' hapA.protein.length.txt
# 24845286
awk '{x+=$2}END{print x/NR}' hapB.protein.length.txt
# 356.414
awk '{x+=$2}END{print x}' hapB.protein.length.txt
# 24970016
```

### transcript and splice variant count
First, files with gene ID and cooresponding counts of transcripts are created
```bash
grep ">" Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.protein.faa | sed 's/>//' | awk '{print $1}' | sed 's/.t/\tt/' | cut -f1 | sort | uniq -c | sed 's/Maldo/\tMaldo/' | awk '{print $2"\t"$1}' > hapA.transcript.count.txt
grep ">" Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.protein.faa | sed 's/>//' | awk '{print $1}' | sed 's/.t/\tt/' | cut -f1 | sort | uniq -c | sed 's/Maldo/\tMaldo/' | awk '{print $2"\t"$1}' > hapB.transcript.count.txt
```
Now calculate the average number of splice vatiants for each gene
```bash
awk '{total += $2; count++} END {print total/count}' hapA.transcript.count.txt 
# 1.29692
awk '{total += $2; count++} END {print total/count}' hapB.transcript.count.txt 
# 1.29177
```

Next, count how many genes have splice variants
```bash
cut -f2 hapA.transcript.count.txt | sort | uniq -c
#  42844 1
#     11 10
#      1 11
#      1 13
#   7028 2
#   1812 3
#    769 4
#    326 5
#    125 6
#     58 7
#     36 8
#     17 9
cut -f2 hapB.transcript.count.txt | sort | uniq -c
#  43870 1. 43870 genes only has one transcript per gene
#     11 10. 11 genes has 10 transcripts per gene.
#      4 11
#      1 12
#   7227 2
#   1833 3
#    779 4
#    282 5
#    121 6
#     51 7
#     43 8
#     13 9
```
The number of genes contain more than one splice variants in hapA is: 53028-42844=10184.  
The number of genes contain more than one splice variants in hapB is: 54235-43870=10365.

### UTR counts
Create sym links to annotation GFF files:
```bash
ln -s ../../08-Files/Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 
ln -s ../../08-Files/Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3
```

Now count the total number of mRNA and the total number of mRNAs with UTR
```bash
cut -f3 Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 | grep -c "mRNA"
# 68773
cat Malus-domestica-WA_38_hapA-genome-v1.0.a1.fa.gff3 | cut -f9 | grep "prime_UTR"| sed 's/ID=//' | sed 's/;.*//' | sed 's/.five.*//' |  sed 's/.three.*//' | sort | uniq | wc -l
# 36773
cut -f3 Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3 | grep -c "mRNA"
# 70059
cat Malus-domestica-WA_38_hapB-genome-v1.0.a1.fa.gff3 | cut -f9 | grep "prime_UTR"| sed 's/ID=//' | sed 's/;.*//' | sed 's/.five.*//' |  sed 's/.three.*//' | sort | uniq | wc -l
# 36576
```


