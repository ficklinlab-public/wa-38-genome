Directory Overview
==================
This directory contains steps of performing structural annotation, including repeat annotation and gene space annotation.  

Authors: Huiting Zhang  
Last Updated: Aug 5th 2024  

Directory Structure
===================
The following describes the purpose for each directory:

### 01-annotate_repeats
Two spteps were used for repeat annotation. EDTA was used to identify repetitive elements and RepeatMasker was used to annotate additional low complexity elements and mask the genome.  
- 01-EDTA
- 02-RepeatMasker
- 03-Telomere

### 02-annotate-genes
BRAKER and TSEBRA was used to perform ab initio and evidence-based gene annotation. PASA was then deployed to polish the gene models and add UTR annotations. A custom script was then used to filter erroneous gene models.  
- 01-GEMmaker  
*GEMmaker was used to align RNA-seq reads to the assemblies*
- 02-Braker1_RNAseq  
*This step performs ab initio prediction and evidence-based annotation using transcriptome data*  
- 03-Braker2_protein  
*This step performs ab initio prediction and evidence-based annotation using homologous proteins*  
- 04-TSEBRA  
*Transcript selector to merge gene models predicted from the 2 predictions*  
- 05-Trinity  
*Create TRINITY de novo and reference-guided transcriptome assemblies, which are used as inputs for PASA*  
- 06-PASA  
*Gene model polishing and UTR annotation*  
- 07-FilterGenes  
*Fixing erronous gene models using a custom script (e.g. removal of overlapping gene models, splitting non-overalpping splice variants into seperate gene models, etc)*  
- 08-Files  
*A custom script was used to generate the sequence fasta files. Genes and repeats were renamed following a naming convention recommended by [GDR](https://www.rosaceae.org/nomenclature/genome)*  
- 09-annotation_stats  
*Basic statisitics (e.g. protein length, number of splice variants per gene, etc) were calculated.*  
- 10-evidence-mapping  
*Each gene was mapped with whether it contains evidence from RNA-seq data, homologous proteins, functional annotation, and gene family. This step is performed after the completion of `../06-functional-annotation` and `../07-comparative`*
